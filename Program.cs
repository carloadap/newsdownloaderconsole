﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess;

namespace NewsDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Controller manager = new Controller();
                //Console.WriteLine("Deleting News");
                //manager.DeleteNews();
                DateTime today = DateTime.Now;
                //getLastWeekNews(manager, today);
                today = getLatestNews(manager, today);
                manager.NewsHistory();
                
            }
            else
            {
                foreach (string date in args)
                {
                    Controller manager = new Controller();
                    //Console.WriteLine("Deleting News");
                    //manager.DeleteNews();
                    DateTime today = Convert.ToDateTime(date);
                    //getLastWeekNews(manager, today);
                    today = getLatestNews(manager, today);
                    manager.NewsHistory();
                }
            }
            Console.ReadLine();
        }

        private static DateTime getLatestNews(Controller manager, DateTime today)
        {
            while (today.DayOfWeek != DayOfWeek.Sunday)
                today = today.AddDays(-1);
            string formattedDate = String.Format("{0:MM-dd-yyyy}", today);
            string weekNews = String.Format(@"http://dailyfx.com/files/Calendar-{0}.csv", formattedDate);
            Console.WriteLine("Downloading the latest news for this week . . . .  ");
            manager.DownloadNews(weekNews);
            Console.WriteLine("Download Complete");
            return today;
        }

        private static void getLastWeekNews(Controller manager, DateTime today)
        {
            DayOfWeek dayToFind = DayOfWeek.Sunday;
            DateTime lastWeek = today.AddDays(-1 * (int)today.DayOfWeek).AddDays(-7).AddDays((int)dayToFind);
            string formattedDate = String.Format("{0:MM-dd-yyyy}", lastWeek);
            string weekNews = String.Format(@"http://dailyfx.com/files/Calendar-{0}.csv", formattedDate);
            Console.WriteLine("Downloading the last week news . . . .  ");
            manager.DownloadNews(weekNews);
            Console.WriteLine("Download Complete");
        }
    }
}
